﻿using System;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace playground.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class AboutPage : ContentPage
    {
        public int UselessMethod(int unusedArgument)
        {
            //afsadfsad
            //asdfsadfasdg
            //asdfasdfasdfsa
            var letsCheckIfSonarWillShowAnythingAboutThisPieceOfShit = Math.Log(1234);
            return (int)(letsCheckIfSonarWillShowAnythingAboutThisPieceOfShit / 8);
        }

        public AboutPage()
        {
            InitializeComponent();

            if (true)
            {
                var a42 = 42;
                a42 = 42;
                System.Diagnostics.Debug.WriteLine(UselessMethod(a42));
            }
            else
            {
                var a42 = 40;
                System.Diagnostics.Debug.WriteLine(UselessMethod(a42));
            }
        }
    }
}